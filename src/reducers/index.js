import { combineReducers } from 'redux'
import page from './page'
import user from './user'

export default combineReducers({
  page,
  user
})


// const initialState = {
//   user: 'Unknown User',
//   surname: 'Petrovitch',
//   age: '51'
// };
// 
// export default function userstate() {  // (state = initialState)
//   return initialState;  // state
// }
