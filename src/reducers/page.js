import {
	GET_PHOTOS_REQUEST,
	GET_PHOTOS_SUCCESS,
	GET_NEW_INC_YEAR
} from '../constants/Page'

const initialState = {
	year: 2016,
	photos: [],
  fetching: false,
	newIncYear: 2015
}

export default function page(state = initialState, action) {

	switch (action.type) {
		case GET_PHOTOS_REQUEST:
			return {...state,
				year: action.payload,
        fetching: true
			}

		case GET_PHOTOS_SUCCESS:
			return {...state,
				photos: action.payload,
        fetching: false
			}

		case GET_NEW_INC_YEAR:
			return {...state,
				newIncYear: action.payload
			}

		default:
			return state;
	}
}
