import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import User from '../components/User'
import Page from '../components/Page'
import * as pageActions from '../actions/PageActions'

class App extends Component {
  render() {
    const { user, page, pageActions } = this.props;

    return <div>
      <User name={user.name} />
      <Page photos={page.photos} year={page.year} fetching={page.fetching} newIncYear={page.newIncYear} pageActions={pageActions}/>
    </div>
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    page: state.page
  }
}

function mapDispatchToProps(dispatch) {
  let actions = bindActionCreators(pageActions, dispatch);
  console.log(actions);
  return {
    pageActions: actions
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
