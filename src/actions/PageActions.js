import {
	GET_PHOTOS_REQUEST,
	GET_PHOTOS_SUCCESS,
	GET_NEW_INC_YEAR
} from '../constants/Page'

export function actionGetPhotos(year) {

	// return {
	// 	type: SET_YEAR,
	// 	payload: year
	// }

  return (dispatch) => {
   dispatch({
     type: GET_PHOTOS_REQUEST,
     payload: year
   })

   setTimeout(() => {
     dispatch({
       type: GET_PHOTOS_SUCCESS,
       payload: [1,2,3,4,5]
     })
   }, 1000)
 }
}

export function actionIncYear(year) {

	return (dispatch) => {
    dispatch({
			type: GET_NEW_INC_YEAR,
			payload: year+1
    })

		setTimeout(() => {
      dispatch({
				type: GET_NEW_INC_YEAR,
				payload: year+2
      })
    }, 1000)
	}
}
