import React, {PropTypes, Component} from 'react'

export default class Page extends Component {

	onYearBtnClick(e) {
		this.props.pageActions.actionGetPhotos(+ e.target.innerText);
	}
  onIncYear() {
		this.props.pageActions.actionIncYear( this.props.year );
	}
	render() {
		const {year, photos, fetching, newIncYear} = this.props;

		return <div>
			<p>
				<button onClick={:: this.onYearBtnClick}>2016</button>
				<button onClick={:: this.onYearBtnClick}>2015</button>
				<button onClick={:: this.onYearBtnClick}>2014</button>
			</p>
			<p>У тебя {photos.length} фото за {year} год
        (_{fetching ? "getingPhotos" : `Done:${photos}`}_)</p>
			<p>
				<button onClick={:: this.onIncYear}>INCrement Year {year}</button>
			</p>
      <p>
        new Incremented Year = {newIncYear}
      </p>
		</div>
	}
}

Page.propTypes = {
	year: PropTypes.number.isRequired,

	photos: PropTypes.array.isRequired,
	fetching: PropTypes.bool.isRequired,
  newIncYear: PropTypes.number.isRequired,

	pageActions: PropTypes.arrayOf(PropTypes.func)
}
